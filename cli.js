#!/usr/bin/env node
const {installModules} = require("./scripts/install-rell-modules");
const config = require('./scripts/module.json');


const [,, ...args] = process.argv;

console.log(`Installing Rell Modules... ${args}`);

(async () => {
    await installModules(config);
})()